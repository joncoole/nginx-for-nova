# NGINX for Nova

Provides syntax highlighting and folding for **NGINX configuration files**.

Some OpenResty (nested Lua) support has been added. Still in progress.

> **NOTE**
> The NGINX tree-sitter implementation is in early development.
>
> If you wish to contribute, you may find the source [here](https://gitlab.com/joncoole/tree-sitter-nginx).


<img src="https://gitlab.com/joncoole/nginx-for-nova/-/raw/main/Images/docs/highlights.png" width="500" height="382">
