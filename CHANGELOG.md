## Version 0.2.6
- Improvements to file name / extension detection, thanks to [Toni Förster](https://gitlab.com/stonerl).
	- His changes resolve activation conflicts with other Nova extensions.

- The former approach was overly broad, as many non-NGINX files can end in “conf”.
	- Methods for external NGINX config detection will be investigated.

## Version 0.2.5
- Added several OpenResty Lua blocks

## Version 0.2.4
- Improvements to regex tokens within unquoted strings

## Version 0.2.3
- Corrected variables parsing within strings

## Version 0.2.2
- Improvements to single & double quote strings
- Improvements to handling parentheses
- Implemented indentation patterns
- Removed some obsolete, overly specific grammars

## Version 0.2.1
- Improved Lua block folding
- Corrected Lua string handling with block characters
- Corrected handling of ':' within upstreams

## Version 0.2
- Added Lua Syntax
- Added Lua injection for `access_by_lua_block` blocks (experimental)
- Improved parsing for Lua blocks
- Improved parsing for ipv4 when variables are used after scheme
	- Long term specific parsing such as this might not be worth it
- Additional highlighting

## Version 0.1.1
- Corrected syntax name
- Updated scheme highlighting
- Updated ipv4 highlighting

## Version 0.1

### Initial release
- Basic NGINX syntax highlighting & folding
