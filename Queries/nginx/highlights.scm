; (simple_directive) @string

(comment) @comment

(number) @value.number
(metric) @value.number

(regex) @regex

(variable) @identifier.variable

(modifier) @keyword.modifier

(simple_directive
	name: (directive) @identifier.function)

(block_directive
	name: (directive) @identifier.function)

(lua_block_directive) @identifier.function

((generic) @value.boolean
	(#match? @value.boolean "^(off|on)$"))

(generic) @string
(string) @string

(scheme) @string
(ipv4) @value.number

[
	";"
] @punctuation.delimiter

[
	"{"
	"}"
	"("
	")"
	"["
	"]"
] @punctuation.bracket

; Lua Debug
(lua_code) @definition.type
